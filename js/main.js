let RUN = true;

let gameTick = 0;

let userX0 = USER.x0;
let userY0 = USER.y0;

async function main() {
  function game() {
    gameTick++;
    // USER.move();
    // USER.x
    if (
      gameTick % (ANIMATION_SPEED / GAME_CYCLE) === 0 ||
      userX0 !== USER.x0 ||
      userY0 !== USER.y0
    ) {
      let x = USER.x + USER.x0;
      let y = USER.y + USER.y0;

      try {
        const index = SCENE_MODEL.earthMap[`${x} ${y}`];
        if (SCENE_MODEL.earth[index].lvl !== 1) {
          throw "";
        }
      } catch (e) {
        x = USER.x;
        y = USER.y;
      }

      userX0 = USER.x0;
      userY0 = USER.y0;

      if (ENEMIES.every((e) => e.x !== x || e.y !== y)) {
        USER.move(x, y);
        mainCamera(
          SCENE_MODEL.width / 4 - USER.x * SIZE,
          SCENE_MODEL.height - USER.y * SIZE
        );
      }
    }

    return sleep(GAME_CYCLE);
  }

  while (RUN) {
    await game();
  }
}

main();
