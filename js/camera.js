let CAMERA_X = 0;
let CAMERA_Y = 0;
let CAMERA_OVERVIEW = 0;

let lastClientX = 0;

mainCamera();

function mainCamera(x = CAMERA_X, y = CAMERA_Y, v = CAMERA_OVERVIEW) {
  SCENE_CAMERA.innerHTML = `
  .earth-3d {
    transform: perspective(${
      2 * window.innerHeight
    }px) rotateX(65deg) rotateZ(${v}deg) translate(${x}px, ${y}px) scale(1.1);
  }`;
}

window.addEventListener("keypress", (e) => {
  if (e.altKey) {
    if (e.code === "KeyW") {
      CAMERA_Y += CAMERA_STEP;
    } else if (e.code === "KeyS") {
      CAMERA_Y -= CAMERA_STEP;
    }

    if (e.code === "KeyA") {
      CAMERA_X += CAMERA_STEP;
    } else if (e.code === "KeyD") {
      CAMERA_X -= CAMERA_STEP;
    }

    mainCamera();
  }
});
